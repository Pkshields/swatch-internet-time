﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace SwatchInternetTime
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : SwatchInternetTime.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            //Create a timer to keep updating the clock on screen
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += UpdateTimer;
            timer.Start();

            ///Fill out the TimeZone ComboBox on the UI
            foreach (TimeZone zone in timeZones)
                TimeZoneCombo.Items.Add(zone.TimeZoneDesc);
            TimeZoneCombo.SelectedIndex = 13;

            //
            stopwatchTimer = new DispatcherTimer();
            stopwatchTimer.Interval = TimeSpan.FromMilliseconds(100);
            stopwatchTimer.Tick += UpdateStopwatchTimer;

            //
            storedTime = 0.0f;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Update the GUI timer on screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void UpdateTimer(object sender, object e)
        {
            //Get the current time
            DateTime time = DateTime.Now;

            //Convert the given DateTime to universal time and add one hour (internet time is based on UTC+1)
            DateTime utc1 = time.ToUniversalTime().AddHours(1);

            //Get the milliseconds of the given day and divide it to the number of seconds of a day
            float beats = (float)(utc1.TimeOfDay.TotalMilliseconds / 86400d);

            //Display the time on screen, in 2 or 4 decimal places
            TimeBox.Text = "@" + beats;//string.Format("{0:0.##}", beats);
        }

                                                #region CONVERTER

        /// <summary>
        /// Simple struct used to hold TimeZone data
        /// </summary>
        private struct TimeZone
        {
            /// <summary>
            /// The hours offset between UTC and this timezone
            /// </summary>
            public float TimeDifference;

            /// <summary>
            /// Short description letting the user know what timezone this is
            /// </summary>
            public string TimeZoneDesc;

            /// <summary>
            /// Create an object of type TimeZone
            /// </summary>
            /// <param name="timeDifference">The hours offset between UTC and this timezone</param>
            /// <param name="timeZoneDesc">Short description letting the user know what timezone this is</param>
            public TimeZone(float timeDifference, string timeZoneDesc)
            {
                TimeDifference = timeDifference;
                TimeZoneDesc = timeZoneDesc;
            }
        }

        /// <summary>
        /// List of hella timezones
        /// </summary>
        TimeZone[] timeZones = {   new TimeZone(-12.0f, "(GMT-12:00) International dateline, west"),
                                   new TimeZone(-11.0f, "(GMT-11:00) Midway Islands, Samoan Islands"),
                                   new TimeZone(-10.0f, "(GMT-10:00) Hawaii"),
                                   new TimeZone(-9.0f, "(GMT-09:00) Alaska"),
                                   new TimeZone(-8.0f, "(GMT-08:00) Pacific Time (USA og Canada); Tijuana"),
                                   new TimeZone(-7.0f, "(GMT-07:00) Mountain Time (USA og Canada)"),
                                   new TimeZone(-6.0f, "(GMT-06:00) Central time (USA og Canada)"),
                                   new TimeZone(-5.0f, "(GMT-05:00) Eastern time (USA og Canada)"),
                                   new TimeZone(-4.0f, "(GMT-04:00) Atlantic Time (Canada)"),
                                   new TimeZone(-3.5f, "(GMT-03:30) Newfoundland"),
                                   new TimeZone(-3.0f, "(GMT-03:00) Brasilia"),
                                   new TimeZone(-2.0f, "(GMT-02:00) Mid-Atlantic"),
                                   new TimeZone(-1.0f, "(GMT-01:00) Azorerne"),
                                   new TimeZone(0.0f, "(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lissabon, London"),
                                   new TimeZone(1.0f, "(GMT+01:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien"),
                                   new TimeZone(2.0f, "(GMT+02:00) Athen, Istanbul, Minsk"),
                                   new TimeZone(3.0f, "(GMT+03:00) Moscow, St. Petersburg, Volgograd"),
                                   new TimeZone(3.5f, "(GMT+03:30) Teheran"),
                                   new TimeZone(4.0f, "(GMT+04:00) Abu Dhabi, Muscat"),
                                   new TimeZone(4.5f, "(GMT+04:30) Kabul"),
                                   new TimeZone(5.0f, "(GMT+05:00) Islamabad, Karachi, Tasjkent"),
                                   new TimeZone(5.5f, "(GMT+05:30) Kolkata, Chennai, Mumbai, New Delhi"),
                                   new TimeZone(5.75f, "(GMT+05:45) Katmandu"),
                                   new TimeZone(6.0f, "(GMT+06:00) Astana, Dhaka"),
                                   new TimeZone(6.5f, "(GMT+06:30) Rangoon"),
                                   new TimeZone(7.0f, "(GMT+07:00) Bangkok, Hanoi, Djakarta"),
                                   new TimeZone(8.0f, "(GMT+08:00) Beijing, Chongjin, SAR Hongkong, Ürümqi"),
                                   new TimeZone(9.0f, "(GMT+09:00) Osaka, Sapporo, Tokyo"),
                                   new TimeZone(9.5f, "(GMT+09:30) Adelaide"),
                                   new TimeZone(10.0f, "(GMT+10:00) Canberra, Melbourne, Sydney"),
                                   new TimeZone(11.0f, "(GMT+11:00) Magadan, Solomon Islands, New Caledonien"),
                                   new TimeZone(12.0f, "(GMT+12:00) Fiji, Kamtjatka, Marshall Islands"),
                                   new TimeZone(13.0f, "(GMT+13:00) Nuku'alofa")
                               };

        /// <summary>
        /// Ensure the SwatchBox is numeric only and can only contain up to 3 characters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConvertSwatchBox_KeyDown_1(object sender, KeyRoutedEventArgs e)
        {
            //Get the key and the box itself
            char key = Convert.ToChar(e.Key);
            TextBox box = (TextBox)sender;

            //If the key is not 0-9 prevent the event from being handled further
            if (!(key >= '0' && key <= '9') || (key == '.'))
                e.Handled = true;
        }
        
        /// <summary>
        /// Button to convert real time to Swatch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimeToSwatchButton_Click(object sender, RoutedEventArgs e)
        {
            //Get the hours and minutes from the respective comboboxes
            int hours = NormalTime1Combo.SelectedIndex;
            int minutes = NormalTime2Combo.SelectedIndex;

            //Get the timezone tmimeoffset from the conbo gbox and respective list
            int timeZoneID = TimeZoneCombo.SelectedIndex;
            TimeZone zone = timeZones[timeZoneID];
            float timeOffset = zone.TimeDifference;
            
            //Conversion!
            DateTime test = new DateTime(2012, 06, 01, hours, minutes, 0);      //Create a DateTime with the hours/minutes info
            test = test.AddHours(-timeOffset);                                  //Apply the hours offset from above to turn the time to UTC
            test = test.AddHours(1);                                            //Then convert to UTC+1 for Swatch
            float beats = (float)(test.TimeOfDay.TotalMilliseconds / 86400d);   //Convert this to Swatch
            ConvertSwatchBox.Text = ""+beats;                                   //Stick that in a box
        }

        /// <summary>
        /// Button to convert Swatch to real time
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SwatchToTimeButton_Click(object sender, RoutedEventArgs e)
        {
            //Convert the beats in the box into a float
            float beats = float.Parse(ConvertSwatchBox.Text);

            //Ensure the box contains a legit number of beats
            if (beats > 1000.0f)
                return;

            //Convert the beats into ticks
            float milliseconds = beats * 86400.0f;
            float ticks = milliseconds * TimeSpan.TicksPerMillisecond;

            //Get the timezone tmimeoffset from the conbo gbox and respective list
            int timeZoneID = TimeZoneCombo.SelectedIndex;
            TimeZone zone = timeZones[timeZoneID];
            float timeOffset = zone.TimeDifference;

            //Conversion!
            DateTime test = new DateTime((long)ticks);      //Create a DateTime with the hours/minutes info
            test = test.AddYears(2012);                     //Bugfix: move to year 2012 oto stop any dumb date errors
            test = test.AddHours(-1);                       //Convert the time to UTC
            test = test.AddHours(timeOffset);               //Apply the hours offset from above to turn the time to the correct timezone
            NormalTime1Combo.SelectedIndex = test.Hour;     //Apply the hours to the hour box
            NormalTime2Combo.SelectedIndex = test.Minute;   //Apply the minutes to the minute box
        }

                                                #endregion CONVERTER

                                                #region STOPWATCH

        /// <summary>
        /// Thing that will update the stopwatch field when required
        /// </summary>
        private DispatcherTimer stopwatchTimer;

        /// <summary>
        /// Stored time, last time soneone hit stop
        /// </summary>
        private float storedTime;

        /// <summary>
        /// Current time at the the the user hit Start
        /// </summary>
        private DateTime currentTime;

        /// <summary>
        /// Update the Stopwatch text fiend with the current stopwatch value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateStopwatchTimer(object sender, object e)
        {
            //Find the difference in time between someone hitting the Start button and now
            TimeSpan difference = DateTime.Now.Subtract(currentTime);

            //Turn that into beats
            float beats = (float)(difference.TotalMilliseconds / 86400d);
            
            //Write that with the stored time to the text field
            StopwatchText.Text = "@" + (storedTime + beats);
        }

        /// <summary>
        /// Start the stopwatch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopwatchStartButton_Click(object sender, RoutedEventArgs e)
        {
            //Reset the currentTime to now & start the timer
            currentTime = DateTime.Now;
            stopwatchTimer.Start();
        }

        /// <summary>
        /// Stop the stopwatch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopwatchStopButton_Click(object sender, RoutedEventArgs e)
        {
            //Get the current stopwatch time and store it, stop the timer
            storedTime = float.Parse(StopwatchText.Text.Substring(1, StopwatchText.Text.Length - 1));
            stopwatchTimer.Stop();
        }

        /// <summary>
        /// Reest the stopwatch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopwatchResetButton_Click(object sender, RoutedEventArgs e)
        {
            //If the timer is going, stop it
            if (stopwatchTimer.IsEnabled)
                stopwatchTimer.Stop();

            //Reset both the storedTime and the textbox
            storedTime = 0;
            StopwatchText.Text = "@000";
        }

                                                #endregion STOPWATCH
    }
}

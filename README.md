# Swatch Internet Time
 
Swatch Internet Time is a revolutionary way to track the time of day, by eliminating the need for time zones and use the same time all over the world. This is the definitive SIT app for Windows 8.

Swatch Internet Time is a Windows 8 Modern app, available on the Windows Store.